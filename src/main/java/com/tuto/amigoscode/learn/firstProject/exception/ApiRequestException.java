package com.tuto.amigoscode.learn.firstProject.exception;

public class ApiRequestException extends RuntimeException{

    // super(message) va chercher le constructeur dans RuntimeException qui possède 1 argument message
    public ApiRequestException(String message) {
        super(message);
    }

    // super(message, cause) va chercher le constructeur dans RuntimeException qui possède
    // 2 argument : message, cause
    public ApiRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
