package com.tuto.amigoscode.learn.firstProject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = {ApiRequestException.class})
    public ResponseEntity<Object> handleApiRequestExceptions(ApiRequestException e){

        HttpStatus badRequest = HttpStatus.BAD_REQUEST;

        // 1. Create object containing the payload of error
        ApiException customApiException = new ApiException(
                e.getMessage(),
                badRequest,
                ZonedDateTime.now(ZoneId.of("Europe/Paris"))
        );

        // 2. Return the response entity
        return new ResponseEntity<>(customApiException, badRequest);
    }
}
