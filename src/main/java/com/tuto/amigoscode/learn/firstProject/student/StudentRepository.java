package com.tuto.amigoscode.learn.firstProject.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("SELECT s FROM Student s WHERE s.email =?1")  // optionnel
        // et Student se réfère à l'objet Student, pas la table.
        // idem s.email se réfère à la propriété email de la classe Student
    Optional<Student> findStudentByEmail(String email);


/*    @Query("SELECT s FROM Student s WHERE s.id = ?1") // optionnel
    Optional<Student> findStudentById(Long studentId);*/
}
