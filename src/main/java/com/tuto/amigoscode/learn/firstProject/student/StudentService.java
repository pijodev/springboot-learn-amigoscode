package com.tuto.amigoscode.learn.firstProject.student;

import com.tuto.amigoscode.learn.firstProject.exception.ApiRequestException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getStudents(){
        return studentRepository.findAll();
    }

    public void addNewStudent(Student student) {
        Optional<Student> optionalStudent =
                studentRepository.findStudentByEmail(student.getEmail());
        if (optionalStudent.isPresent()){
            throw new ApiRequestException("This email is already taken : " + student.getEmail());
        }
        studentRepository.save(student);
    }

    public void deleteStudentById(Long studentId) {
        boolean studentExists = studentRepository.existsById(studentId);
        if (!studentExists){
            throw new ApiRequestException("Student not found with ID : " + studentId);
        }
        studentRepository.deleteById(studentId);
    }

    @Transactional
    public Student updateStudentById(Long studentId, String name, String email) {
        // Note : ici on n'a pas besoin de dire que studentToUpdate est un Optional<Student>
        // grâce à .orElseThrow() car cette méthode jette une exception si findById() renvoie null.
        // Donc si studentToUpdate existe, ça sera forcément non null ;)
        Student studentToUpdate =
                studentRepository.findById(studentId)
                        .orElseThrow(() -> new ApiRequestException(
                "Cannot update student because student does not exist with ID : " + studentId));

        if (name != null
                && name.length() > 0
                && !Objects.equals(studentToUpdate.getName(), name)) {
            // Si le nom est non null et non vide, et que le nouveau nom est différent de l'ancien
            studentToUpdate.setName(name);
        }
        if (email != null
                && email.length() > 0
                && !Objects.equals(studentToUpdate.getName(), email)) {
            Optional<Student> optionalStudent = studentRepository.findStudentByEmail(email);
            if (optionalStudent.isPresent()){
                throw new ApiRequestException("This email is already taken : " + email);
            }
            studentToUpdate.setEmail(email);
        }
        studentRepository.save(studentToUpdate);
        return studentToUpdate;
    }
}