package com.tuto.amigoscode.learn.firstProject.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/student")
public class StudentController {

    private final StudentService studentService; // Reference
    @Autowired // Instancie un new StudentService() comme ça le constructeur peut marcher
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping()
    public List<Student> getStudents(){
        return studentService.getStudents();
    }

    @PostMapping
    public void registerNewStudent(@RequestBody Student student){
        System.out.println("I RECEIVED A NEW OBJECT : " + student.toString());
        studentService.addNewStudent(student);
    }

    @DeleteMapping(path = "/{studentId}")
    public void deleteStudentById(@PathVariable("studentId") Long studentId){
        studentService.deleteStudentById(studentId);
    }
    @PutMapping(path = "/{studentId}")
    public Student updateStudentById(
            @PathVariable("studentId") Long studentId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String email
            ){
        return studentService.updateStudentById(studentId, name, email);
    }

}
