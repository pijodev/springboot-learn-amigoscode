package com.tuto.amigoscode.learn.firstProject.student;


import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.List;

// Classe pour remplir la base lorsque l'application démarre
@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository) {
        // Créer des étudiants
        return args -> {
            Student student1 = new Student(
                    "Ivan",
                    "ivan@me.com",
                     LocalDate.of(1995, 6, 10)
            );
            Student student2 = new Student(
                    "Bé",
                    "be@me.com",
                    LocalDate.of(1993, 11, 15)
            );

            // Sauver en base
            studentRepository.saveAll(List.of(student1, student2));
        };
    }
}
