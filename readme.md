# Bienvenue dans ce projet SpringBoot

Source : Amigoscode
https://www.youtube.com/watch?v=9SGDpanrc8U&list=PLwvrYc43l1MzeA2bBYQhCWr2gvWLs9A7S&index=2

Objectifs :
Découvrir le framework SpringBoot avec :
- Une API 
- 1 controller
- 1 service
- 1 base de donnée Postgres 
- De l'accès aux données avec JPA
- De la gestion d'erreur

# Spring Initializr

https://start.spring.io/#!type=maven-project&language=java&platformVersion=3.0.2&packaging=jar&jvmVersion=17&groupId=com.tuto.amigoscode.learn&artifactId=firstProject&name=firstProject&description=1st Demo project for Spring Boot&packageName=com.tuto.amigoscode.learn.firstProject&dependencies=web,data-jpa,postgresql

(Lien trop long, retrouvez le dans la source YT dans la description, il faut tout copier coller)


# Schémas directeur du projet :

## 1 - Architecture de l'application
![Application architecture](./src/main/assets/schemas/schema-student_application-architecture.png)

## 2 - Student class representation
![Student class modelisation](./src/main/assets/schemas/schema-student_class-model.png)

# Annotations utilisées, mécanismes / concepts abordés :

## Java Basics
- Packages
- POJOs
- application.properties => connect DB
- Call API => Controller => Service => DAL (schema 1)
- Error Handling
- Maven lifecycle
- Package & run app (.jar for multi-instance)

## Annotations Spring Boot
- @RestController
- @RequestMapping, @GetMapping, @PutMapping, @PostMapping, @DeleteMapping
- @Component
- @Autowired

## JPA
- @Entity
- @Table
- @Repository (Interface)
- Data Access Layer
- @Configuration & @Bean : inject data on startup
- @Transient


# Tests

Il n'y a pas de tests réalisés pour ce projet.